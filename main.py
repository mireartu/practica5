from temperatureConversion import *


def work(flag):
    if flag != True:
        SS = ["C", "F", "K"]
        value = (input("Введите значение ° - "))
        if not str(value).isdigit():
            print("Вы должны ввести число, введите еще раз ")
            return work(False)
        value = float(value)

        _from = str(input("Введите исходную шкалу (C, F, K) - "))

        if _from not in SS:
            print("Введите необходимую исходную шкалу (C, F, K) ")
            return work(False)

        to_ = str(input("Введите шкалу в которую необходимо перевести значение (C, F, K ) - "))

        if to_ not in SS:
            print("Введите верную шкалу для перевода значения (C, F, K)")

            return work(False)

        answer = (str(convert(value, _from, to_)))
        print(answer)
      # with open("test.txt") as file:
          #  file.write(answer)
         #   file.write("\n")
        condition = str(input("Продолжить ? (T / F) ? "))
        conditionKeys = ["T", "t", "True", "TRUE"]
        if condition not in conditionKeys:
            return work(True)
        else:
            return work(False)

    else:
        return "END"


print(work(False))
