from temperatureConversion import convert

value = 30
value2 = 86
value3 = 303.15
C = "C"
F = "F"
K = "K"

def test_convertCtoF():
    result = convert(value, C, F)
    if(result == 86):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n"+ str(value) +"°C ->"+ str(result) +"°F\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")

def test_convertFtoC():
    #assert convert(value2, F, C) == 30
    result = convert(value2, F, C)
    if (result == 30):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n" + str(value2) + "°F -> "+ str(result) +"°C\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")

def test_convertCtoK():
    #assert convert(value, C, K) == 303
    result = convert(value, C, K)
    if (result == 303):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n" + str(value) + "°C -> "+ str(result) +"°K\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")

def test_convertKtoC():
    #assert convert(value3, K, C) == 30
    result = convert(value3, K, C)
    if (result == 30):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n" + str(value3) + "°K -> "+ str(result) +"°C\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")

def test_convertFtoK():
   #assert convert(value2, F, K) == 303
    result = convert(value2, F, K)
    if (result == 303):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n" + str(value2) + "°F -> "+ str(result) +"°K\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")

def test_convertKtoF():
    #assert convert(value3, K, F) ==
    result = convert(value3, K, F)
    if (result == 86):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n" + str(value3) + "°K -> "+ str(result) +"°F\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")

def test_convertCtoC():
    #assert convert(value, C, C) == 30
    result = convert(value, C, C)
    if (result == 30):
        with open("test.txt", "a") as file:
            file.write("УСПЕШНО\n" + str(value) + "°C -> "+ str(result) +"°C\n")
    else:
        with open("test.txt", "a") as file:
            file.write("ОШИБКА")